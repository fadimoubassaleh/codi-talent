const express = require('express');
const bodyparser = require('body-parser');
const api_key = process.env.MAILGUN_API;
const domain = process.env.MAILGUN_DOMAIN;
const mailgun_js = require('mailgun-js');
const cors = require('cors');
const app = express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));
app.use(cors());

app.post('/api/form', (req, res) => {
    const mailgun = mailgun_js({ apiKey: api_key, domain: domain });
    console.log(req.body);
    const htmlEmail = `
            <h2>Student: ${req.body.student_name}</h2>
            <h3>Contact Details</h3>
            <ul>
                <li>Name: ${req.body.name}</li>
                <li>Email: ${req.body.email}</li>
                <li>User Name: ${req.body.username}</li>
            </ul>
            <h3>Message</h3>
            <p>${req.body.message}</p>
        `;

    const mailgun_data = {
        from: 'Backend Talent <contact@talents.codi.tech>',
        to: process.env.TO || 'info@codi.tech',
        subject: 'Contact For on Codi Talent website',
        text: req.body.message,
        html: htmlEmail
    };

    mailgun.messages().send(mailgun_data, function (error, body) {
        console.log(body);
        if (error !== undefined) {
            return res.send({ success: false })
        }
        return res.send({ success: true })

    });
});

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
    console.log(`server listening on port ${PORT}`)
});
