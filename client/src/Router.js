// modules imports
import React from 'react'
import { Switch, Route } from 'react-router-dom'
// files imports
import PageIntro from './pages/PageIntro';
import PageStudent from './pages/PageStudent';

class Router extends React.Component {
    render() {
        return (
            <Switch>
                <Route exact path='/Home' component={PageStudent} />
                <Route exact path='/' component={PageIntro} />
            </Switch>
        )
    }
}

export default Router
